export interface FileTreeNode {
  /** The file name */
  name: string,
  /** Expects 1 for folder, 2 for file */ 
  type: number,
  /** If a folder, it should have child FileTreeNode array. */
  children?: FileTreeNode[]
}