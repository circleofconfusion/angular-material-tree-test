import { Component } from '@angular/core';
import { FileTreeNode } from 'src/models/FileTreeNode';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-material-tree-test';
  selectedFile: FileTreeNode;
  files: FileTreeNode[] = [
    {
      name: 'PDF',
      type: 1,
      children: [
        {
          type: 2,
          name: 'PDF document.pdf',
        }
      ]
    },
    {
      name: 'Office documents',
      type: 1,
      children: [
        {
          name: 'Word',
          type: 1,
          children: [
            {
              type: 2,
              name: 'legacy Word document.doc'
            },
            {
              type: 2,
              name: 'legacy Word template.dot'
            },
            {
              type: 2,
              name: 'legacy Word document backup.wbk'
            },
            {
              type: 2,
              name: 'Word document.docx'
            },
            {
              type: 2,
              name: 'Word document with macros.docm'
            },
            {
              type: 2,
              name: 'Word template.dotx'
            },
            {
              type: 2,
              name: 'Word document binary.docb'
            }
          ]
        },
        {
          name: 'Excel',
          type: 1,
          children: [
            {
              type: 2,
              name: 'legacy Excel.xls'
            },
            {
              type: 2,
              name: 'legacy Excel template.xlt'
            },
            {
              type: 2,
              name: 'legacy Excel with macros.xlm'
            },
            {
              type: 2,
              name: 'Excel.xlsx'
            },
            {
              type: 2,
              name: 'Excel with macros.xlsm'
            },
            {
              type: 2,
              name: 'Excel template.xltx'
            },
            {
              type: 2,
              name: 'Excel template with macros.xltm'
            },
            {
              type: 2,
              name: 'Excel binary.xlsb'
            },
            {
              type: 2,
              name: 'Excel add-in.xla'
            },
            {
              type: 2,
              name: 'Excel macro-enabled add-in.xlam'
            },
            {
              type: 2,
              name: 'Excel dll add-in.xll'
            },
            {
              type: 2,
              name: 'Excel work space.xlw'
            }
          ]
        },
        {
          name: 'PowerPoint',
          type: 1,
          children: [
            {
              type: 2,
              name: 'legacy PowerPoint presentation.ppt'
            },
            {
              type: 2,
              name: 'legacy PowerPoint template.pot'
            },
            {
              type: 2,
              name: 'legacy PowerPoint slideshow.pps'
            },
            {
              type: 2,
              name: 'PowerPoint presentation.pptx'
            },
            {
              type: 2,
              name: 'PowerPoint macro-enabled presentation.pptm'
            },
            {
              type: 2,
              name: 'PowerPoint template.potx'
            },
            {
              type: 2,
              name: 'PowerPoint macro-enabled template.potm'
            },
            {
              type: 2,
              name: 'PowerPoint add-in.ppam'
            },
            {
              type: 2,
              name: 'PowerPoint slideshow.ppsx'
            },
            {
              type: 2,
              name: 'PowerPoint macro-enabled slideshow.ppsm'
            },
            {
              type: 2,
              name: 'PowerPoint slide.sldx'
            },
            {
              type: 2,
              name: 'PowerPoint macro-enabled slide.sldm'
            }
          ]
        }
      ]
    },
    {
      name: 'empty folder',
      type: 1
    },
    {
      name: 'file2.doc',
      type: 2
    }
  ];

  onFileSelect(node: FileTreeNode) {
    this.selectedFile = node;
  }
}
