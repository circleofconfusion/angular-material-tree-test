import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import {NestedTreeControl} from '@angular/cdk/tree';
import { FileTreeNode } from '../../models/FileTreeNode';
import {
  faFolderPlus,
  faFolderMinus,
  faFolder,
  faFile,
  faFileWord,
  faFilePdf,
  faFilePowerpoint,
  faFileExcel,
  faFileArchive,
} from '@fortawesome/free-solid-svg-icons';



@Component({
  selector: 'app-file-tree',
  templateUrl: './file-tree.component.html',
  styleUrls: ['./file-tree.component.css']
})
export class FileTreeComponent {
  
  @Input() files: MatTreeNestedDataSource<FileTreeNode>;
  @Output() fileSelect = new EventEmitter<FileTreeNode>();

  treeControl: NestedTreeControl<FileTreeNode> = new NestedTreeControl<FileTreeNode>(node => node.children);

  folderPlusIcon = faFolderPlus;
  folderMinusIcon = faFolderMinus;

  hasChild(_: number, node: FileTreeNode) {
    return !!node.children && node.children.length > 0;
  }

  getLeafIcon(node: FileTreeNode) {
    const name = node.name.trim();
    const type = node.type;

    if (type === 1) return faFolder;

    if (/\.(doc|dot|wbk|docx|docm|dotx|docb)$/i.test(name)) {
      return faFileWord;
    } else if (/\.(xls|xlm|xlt|xlsx|xlsm|xltx|xltm|xlsb|xla|xlam|xll|xlw)$/i.test(name)) {
      return faFileExcel;
    } else if (/\.(ppt|pot|pps|pptx|pptm|potx|potm|ppam|ppsx|ppsm|sldx|sldm)/i.test(name)) {
      return faFilePowerpoint;
    } else if (/\.pdf$/.test(name)) {
      return faFilePdf;
    } else if (/\.(zip|zipx|tar|tgz|gz|bz2|7z|s7z|rar)$/i.test(name)) {
      return faFileArchive;
    } else {
      return faFile;
    }
  }

  onClickNode(node: FileTreeNode) {
    this.fileSelect.emit(node);
  }
} 
