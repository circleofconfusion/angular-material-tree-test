# Angular Material Tree Test

A test of Angular Material's MatTree Component.

One thing I really like about this library is that the data driving the view and the view 
presentation is not mixed. The data structure the expected from the server for use in the
FileTreeComponent I made is modeled in the FileTreeNode interface.
This will probably need to be tweaked for real-life use, but I don't expect the 
MatTree component library to give us any trouble.

I tested this for basic screen reader and keyboard accessbility, and tweaked my HTML accordingly.

## Running

Install all dependencies with npm with the console command:

    npm install

You may need to run a build first. For some reason Angular's server didn't pick up all the
dependencies right away, throwing an error, but after running a build, this was no longer a problem.

    ng build

Serve an Angular web page:

    ng serve

Open your favorite web browser to localhost:4200
